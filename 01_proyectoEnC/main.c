//
//  main.c
//  01_proyectoEnC
//
//  Created by Sebastián Mejía Ríos on 16/09/21.
//
//  Este programa convierte las coordenadas geográficas Sexagesimales en Decimales
//  Debe ingresarse los grados, minutos, segundos y decimales de manera separada


#include <stdio.h>

int main(int argc, const char * argv[]) {
    
    int gn, ge, mn, me, sn, se;
    float dn, de, sdn, sde;
    double coordenadaN, coordenadaE;
    
    printf("Ingresa la coordenada de latitud - Norte -\n");
    
    printf("ingresa Grados: ");
    scanf("%i", &gn);
    
    printf("ingresa Minutos: ");
    scanf("%i", &mn);
    
    printf("ingresa Segundos: ");
    scanf("%i", &sn);
    
    printf("ingresa Decimales: ");
    scanf("%f", &dn);
    
    if(dn <= 9){
        sdn = sn + (dn/10);
        
    }else{
        sdn = sn + (dn/100);
    }
    
    printf("Ingresa la coordenada de longitud - Oeste -\n");
    
    printf("ingresa Grados: ");
    scanf("%i", &ge);
    
    printf("ingresa Minutos: ");
    scanf("%i", &me);
    
    printf("ingresa Segundos: ");
    scanf("%i", &se);
    
    printf("ingresa Decimales: ");
    scanf("%f", &de);
    
    if(de <= 9){
        sde = se + (de/10);
        
    }else{
        sde = se + (de/100);
    }
    
    
    printf("La coordenada norte ingresada es: %iº %i' %f'' \n", gn, mn, sdn);
    printf("La coordenada norte ingresada es: %iº %i' %f'' \n", ge, me, sde);
    
    coordenadaN = gn + (mn/60.0) + (sdn/3600.0);
    coordenadaE = ge + (me/60.0) + (sde/3600.0);
    printf("la coordenada latitud en decimal es:\t %f \n", coordenadaN);
    printf("la coordenada longitud en decimal es:\t %f \n", coordenadaE);
    
    char user[] = "Sebastián Mejía Ríos";
    printf("Programa creado por %s \n", user);
    
    return 0;
}
